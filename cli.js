#!/usr/bin/env node

'use strict';

const { run } = require('./index'),
    argv = require('yargs').argv;

(async () => {
    try {
        const files = _extractFilesParam();
        if (!files.length) {
            throw new Error('--files param must be provided with at least one path');
        }

        await run(files, argv.distFolder, argv.distFile);
        process.exit(0);
    } catch(err) {
        console.error(err);
        process.exit(1);
    }
})();

function _extractFilesParam() {
    let files = argv.files;
    if (!files) {
        files = [];
    } else if (!Array.isArray(files)) {
        files = [files];
    }
    return files;
}