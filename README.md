# sp-os-builder

Provides a builder (concat and minify) for a standard sp os package.

### Usage

Run the following command in your terminal:

The node way:
```
node path/to/builder/cli.js --files="file/to/build.js" --files="second/file/to/build.js"
```
The local npm way:
```
"./node_modules/.bin/sp-os-builder" --files="file/to/build.js" --files="second/file/to/build.js"
```
The global npm way:
```
sp-os-builder --files="file/to/build.js" --files="second/file/to/build.js"
```


##### Command options:
* `files`
Required.
Determines the paths to be bundled.
* `dist-folder`
Optional.
Determines the name of the generated dist folder.
The default value is `dist`.
* `dist-file`
Optional.
Determines the name of the generated dist files.
The default value is the name in the package.json file or `dist`.