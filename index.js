'use strict';

const fs = require('fs'),
    path = require('path'),
    Promise = require('bluebird'),
    uglifyJS = require('uglify-js'),
    CleanCSS = require('clean-css');

// TODO add git add for dist files
module.exports = {
    run
};

/**
 * Run os builder
 * @public
 *
 * @param {Array<string>} files
 * @param {string} [distDir]
 * @param {string} [distFile]
 *
 * @return {Promise<void>}
 */
async function run(files, distDir, distFile) {
    if (!files || !files.length) {
        throw new Error('Files must be provided with at least one path');
    }

    const distFolder = distDir || './dist',
        distFileName = await _getDistFileName(distFile);

    await _deletePath(distFolder);
    await _mkDir(distFolder);

    await _appendFiles(distFolder, distFileName, files);
}

async function _appendFiles(distFolder, distFileName, files) {
    for (const file of files) {
        const stats = await _getStats(file);
        if (!stats) {
            continue;
        }
        if (stats.isDirectory()) {
            const names = await _readDir(file);
            await _appendFiles(distFolder, distFileName, names.map(n => path.join(file, n)));
        } else {
            const content = await _readFile(file);
            const extName = path.extname(file);
            await _appendFile(path.join(distFolder, `${distFileName}${extName}`), content);
            if (extName === '.js') {
                await _appendFile(path.join(distFolder, `${distFileName}.min.js`), _minifyJs(file, content));
            }
            if (extName === '.css') {
                await _appendFile(path.join(distFolder, `${distFileName}.min.css`), _minifyCss(file, content));
            }
        }
    }
}

async function _getDistFileName(distFile) {
    if (distFile) {
        return distFile;
    }

    try {
        const packageJson = JSON.parse(await _readFile('package.json'));
        if (packageJson.name) {
            return packageJson.name;
        }
    } catch(e) {}

    return 'dist';
}

function _minifyJs(file, code) {
    const minify = uglifyJS.minify(code);

    if (minify.error) {
        throw new Error(`Minify Error: ${minify.error.message}\nat ${path.join(process.cwd(), file)}:${minify.error.line}:${minify.error.col}`)
    }

    return minify.code;
}

function _minifyCss(file, code) {
    const minify = new CleanCSS({compatibility: 'ie9'}).minify(code);

    if (minify.errors && minify.errors.length) {
        const message = typeof minify.errors[0] === 'string' ? minify.errors[0] : JSON.stringify(minify.errors[0]);
        throw new Error(`Clean CSS Error: ${message}\non file ${path.join(process.cwd(), file)}`);
    }
    if (minify.warnings && minify.warnings.length) {
        for (const warning of minify.warnings) {
            console.log(`CSS warning in file ${path.join(process.cwd(), file)}: ${warning}`);
        }
    }

    return minify.styles;
}

async function _deletePath(deletePath) {
    const stats = await _getStats(deletePath);
    if (!stats) {
        return;
    }

    if (stats.isDirectory()) {
        const names = await _readDir(deletePath);
        for (const name of names) {
            await _deletePath(path.join(deletePath, name));
        }

        await _deleteDir(deletePath);
    } else {
        await _deleteFile(deletePath);
    }
}

function _mkDir(mkPath) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.join(process.cwd(), mkPath), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function _readDir(readDirPath) {
    return new Promise((resolve, reject) => {
        fs.readdir(path.join(process.cwd(), readDirPath), (err, names) => {
            if (err) {
                reject(err);
            } else {
                resolve(names);
            }
        });
    });
}

function _deleteDir(rmDirPath) {
    return new Promise((resolve, reject) => {
        fs.rmdir(path.join(process.cwd(), rmDirPath), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function _deleteFile(unlinkPath) {
    return new Promise((resolve, reject) => {
        fs.unlink(path.join(process.cwd(), unlinkPath), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function _getStats(statsPath) {
    return new Promise((resolve, reject) => {
        fs.lstat(path.join(process.cwd(), statsPath), (err, stats) => {
            if (err && (!err.code || err.code !== 'ENOENT')) {
                reject(err);
            } else {
                resolve(stats);
            }
        });
    });
}

function _readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(process.cwd(), filePath), 'utf8', (err, content) => {
            if (err) {
                reject(err);
            } else {
                resolve(content);
            }
        });
    });
}

function _appendFile(filePath, content) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(process.cwd(), filePath), content, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}